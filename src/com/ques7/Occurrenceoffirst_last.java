package com.ques7;

public class Occurrenceoffirst_last {

	public static void main(String[] args) {
		
		String str="Hello, World";
		int firstindexof_o=str.indexOf('o');
		int lastindexof_o=str.lastIndexOf('o');
		int firstindexof_comma=str.indexOf(',');
		int lastindexof_comma=str.lastIndexOf(',');
		System.out.println("first_o:"+firstindexof_o);
		System.out.println("last_o:"+lastindexof_o);
		System.out.println("first_,:"+firstindexof_comma);
		System.out.println("last_,:"+lastindexof_comma);

	}

}
