package com.ques3;

abstract class Shape {

	abstract void RectangleArea();  
	abstract void SquareArea(); 
	abstract void CircleArea();  
	
}
