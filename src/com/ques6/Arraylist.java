package com.ques6;

import java.util.ArrayList;

public class Arraylist {

	public static void main(String[] args) {
		
		  ArrayList<String> list=new ArrayList<String>();   
	      list.add("Pepsi");
	      list.add("Slice");    
	      list.add("Coke");    
	      list.add("Miranda"); 
	      list.add("lemon juice"); 
	      
	      System.out.println("size:"+list.size());  
	      System.out.println("contents:"+list);
	      list.remove(1);
	      System.out.println("contents:"+list);
	      boolean a=list.contains("Coke");
	      System.out.println("Coke is present or not:"+a);
	}

}
