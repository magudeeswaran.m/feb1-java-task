package com.ques2;

abstract class Telephone {
	
	abstract void with();  
	abstract void lift(); 
	abstract void disconnected(); 
	
}
